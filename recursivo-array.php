<?php 

	/**
	 * @method recursive($array = array())
	 * Função responsável por criar uma árvore de arrays predecessores (pais e filhos)
	 * @param  array $array (contém o array incial com todas as variáves seguindo a estrutura do exemplo)
	 * @return array multidimensional
	 */
	function recursive($array = array())
	{	
		function montaArray($ArrayFinal, $ArrayCompare)
		{	
			if (!empty($ArrayFinal))
				if (array_key_exists($ArrayCompare['cod_tarefa_pai'], $ArrayFinal)) 
					$ArrayFinal[$ArrayCompare['cod_tarefa_pai']]['data'][$ArrayCompare['cod_tarefa']]  = $ArrayCompare;
				else 
				{
					$array = current($ArrayFinal);

					foreach ($array as $value)
						if (is_array($value))
						{
							$array     = current($value);
							$resultado[$array['cod_tarefa_pai']]['data'] = montaArray($value, $ArrayCompare);
							$resultado = array_replace_recursive($ArrayFinal, $resultado);
						}
				}
			
			if (isset($resultado)) return $resultado; 
			return $ArrayFinal;
		}
		
		/**
		 * Verificar se existe um array predecessor ao mesmo
		 */
		if (!empty($array))
			foreach ($array as $key => $value) 
				if (is_null($value['cod_tarefa_pai']) ? $ArrayFinal[$key] = $value : $ArrayFinal  = montaArray($ArrayFinal, $value));
		return $ArrayFinal; 
	}

?>