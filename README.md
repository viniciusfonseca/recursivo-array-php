# Recursivo Array PHP

#### Return ``array``

```php

$result[1]['ds_tarefa']			= 'Tarefa 1'; 
$result[1]['cod_tarefa']		= 1;
$result[1]['cod_tarefa_pai']	= null;

$result[2]['ds_tarefa']			= 'Tarefa 2'; 
$result[2]['cod_tarefa']		= 2;
$result[2]['cod_tarefa_pai']	= 1;

$result[3]['ds_tarefa']			= 'Tarefa 3'; 
$result[3]['cod_tarefa']		= 3;
$result[3]['cod_tarefa_pai']	= 2;

$result = recursive($result);

Array
(
    [1] => Array
        (
            [ds_tarefa] => Tarefa 1
            [cod_tarefa] => 1
            [cod_tarefa_pai] => 
            [data] => Array
                (
                    [2] => Array
                        (
                            [ds_tarefa] => Tarefa 2
                            [cod_tarefa] => 2
                            [cod_tarefa_pai] => 1
                            [data] => Array
                                (
                                    [3] => Array
                                        (
                                            [ds_tarefa] => Tarefa 3
                                            [cod_tarefa] => 3
                                            [cod_tarefa_pai] => 2
                                        )

                                )

                        )

                )

        )

)
```